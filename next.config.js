const {
  PHASE_PRODUCTION_BUILD,
  PHASE_PRODUCTION_SERVER,
} = require("next/constants");

module.exports = (phase) => {
  return {
    reactStrictMode: true,
    basePath: "",
    // разкомментить перед билдом в прод. Не поддерживается turbopack, поэтому закомментил

    experimental: {
      // instrumentationHook: true
    },
    typescript: {
      // !! WARN !!
      // Dangerously allow production builds to successfully complete even if
      // your project has type errors.
      // !! WARN !!
      ignoreBuildErrors: true,
    },
  };
};
